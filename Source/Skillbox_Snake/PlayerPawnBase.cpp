// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	pawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Pawn Camera"));
	RootComponent = pawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));
	createSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::handlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::handlePlayerHorizontalInput);
}

void APlayerPawnBase::createSnakeActor(){
	//snakeActorClass ������ ASnakeBase::StaticClass()
	snakeActor = GetWorld()->SpawnActor<ASnakeBase>(snakeActorClass, FTransform());
}

void APlayerPawnBase::handlePlayerVerticalInput(float value){
	if (IsValid(snakeActor)) {
		if (value > 0 && snakeActor->lastMovementDirection != EMovementDirection::DOWN)
			snakeActor->lastMovementDirection = EMovementDirection::UP;
		else if (value < 0 && snakeActor->lastMovementDirection != EMovementDirection::UP)
			snakeActor->lastMovementDirection = EMovementDirection::DOWN;
	}
}

void APlayerPawnBase::handlePlayerHorizontalInput(float value){
	if (IsValid(snakeActor)) {
		if (value > 0 && snakeActor->lastMovementDirection != EMovementDirection::LEFT)
			snakeActor->lastMovementDirection = EMovementDirection::RIGHT;
		else if (value < 0 && snakeActor->lastMovementDirection != EMovementDirection::RIGHT)
			snakeActor->lastMovementDirection = EMovementDirection::LEFT;
	}
}


