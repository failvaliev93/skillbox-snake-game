// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	elementSize = 100.f;
	lastMovementDirection = EMovementDirection::UP;
	movementSpeed = 10.f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(movementSpeed);
	addSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	move();
}

void ASnakeBase::addSnakeElement(int elementsNum){
	for (int i = 0; i < elementsNum; ++i) {
		FVector location(snakeElements.Num() * elementSize, 0, 0);
		FTransform transform(location); // = FTransform(GetActorLocation() - location);
		ASnakeElementBase * newElem = GetWorld()->SpawnActor<ASnakeElementBase>(snakeElementClass, transform);
		//����������� � "������" �����
		//newElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		int32 index = snakeElements.Add(newElem);
		if (index == 0) {
			newElem->setFirstElementType();
		}
	}
}

void ASnakeBase::move(){
	FVector movementVector;
	float movementSpeed1 = elementSize;

	switch (lastMovementDirection)
	{
	case EMovementDirection::UP:
		movementVector.X += movementSpeed1;
		break;
	case EMovementDirection::DOWN:
		movementVector.X -= movementSpeed1;
		break;
	case EMovementDirection::LEFT:
		movementVector.Y += movementSpeed1;
		break;
	case EMovementDirection::RIGHT:
		movementVector.Y -= movementSpeed1;
		break;
	default:
		break;
	}

	//AddActorWorldOffset(movementVector);

	//���������� �����
	for (int i = snakeElements.Num() - 1; i > 0; --i){
		/*auto curElem = snakeElements[i];
		auto prevElem = snakeElements[i - 1];
		FVector prevLoc = prevElem->GetActorLocation();
		curElem->SetActorLocation(prevLoc);*/

		snakeElements[i]->SetActorLocation(snakeElements[i - 1]->GetActorLocation());
	}

	//���������� ������
	snakeElements[0]->AddActorWorldOffset(movementVector);
}

