// Copyright Epic Games, Inc. All Rights Reserved.

#include "Skillbox_Snake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Skillbox_Snake, "Skillbox_Snake" );
