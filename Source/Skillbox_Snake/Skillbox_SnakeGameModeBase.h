// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Skillbox_SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOX_SNAKE_API ASkillbox_SnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
